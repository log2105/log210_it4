var dataBaseManager = require("./DatabaseManager");
var fabriqueUtilisateur = require("./public/javascript/FabriqueUtilisateur").FabriqueUtilisateur;


var userManager = {
		/**
		 * Inscrit un utilisateur.
		 * @param prenom
		 * @param nom
		 * @param dateNaissance
		 * @param adresse
		 * @param telephone
		 * @param motDePasse
		 * @param courriel
		 * @param callback fonction appelée après la fin de l'éxécution par le DatabaseManager
		 */
		inscrireUtilisateur: function(prenom, nom, dateNaissance, adresse, telephone, motDePasse, courriel, type, callback){
			
			dataBaseManager.inscrireUser(prenom, nom, dateNaissance, adresse, telephone, motDePasse, courriel, type, function(){
				
				userManager.authentifierUtilisateur(courriel, motDePasse, callback);
			});
			
			
			
		},
		/**
		 * Vérifie si le compte de l'utilisateur est déjà existant. Renvoi un objet de type utilisateur à la fonction callback.
		 * @param courriel
		 * @param motDePasse
		 * @param callback fonction appelée après la fin de l'éxécution par le DatabaseManager
		 */
		authentifierUtilisateur: function(courriel, motDePasse, callback){


            dataBaseManager.getInfo(courriel, function(monUtilisateur){

                if(monUtilisateur !== null){

                    if(monUtilisateur.motDePasse == motDePasse) {

                        callback(monUtilisateur);
                    }

                }
            });


        },
		/**
		 * Modifie un utilisateur déjà existant.
		 * @param courriel
		 * @param adresse
		 * @param telephone
		 * @param motDePasse
		 * @param callback fonction appelée après la fin de l'éxécution par le DatabaseManager
		 */
		modifierUtilisateur: function(courriel, adresse, telephone, motDePasse, callback){
			dataBaseManager.modifierUser(courriel, adresse, telephone, motDePasse, function(){
				userManager.authentifierUtilisateur(courriel, motDePasse, callback);
			});
			
		},


		getListeRestaurateur: function(callback){
			dataBaseManager.getListeRestaurateur(function(resultat){
				callback(resultat);
			})
			
		},

		supprimerUtilisateur: function(ID, callback){
			dataBaseManager.supprimerUser(ID, function(resultat){
				callback(resultat);
			})
		},

		getInfo: function(ID, callback){
			dataBaseManager.getInfo(ID, function(resultat){
				callback(resultat);
			})
		},
		
		getCarnetAdresse: function(courriel, callback){
			dataBaseManager.getCarnetAdresse(courriel, function(carnet){
				callback(carnet);
			});
		},
		
        getUser: function(courriel, callback){
            dataBaseManager.getInfo(courriel, function(monUtilisateur){
                callback(monUtilisateur);
            });
    }
};

exports.userManager = userManager;
