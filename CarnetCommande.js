/**
 * Created by Frank on 2015-03-07.
 */
var dataBaseManager = require("./DatabaseManager");
var restaurantManager = require("./RestaurantManager").restaurantManager;
var userManager = require("./UserManager").userManager;
var nodemailer = require('nodemailer');

var carnetCommande = {

	ajouterCommande : function(commande, callback){
        dataBaseManager.ajouterCommande(commande, function() {
            IDClient = commande.client;
            userManager.getUser(IDClient, function (unClient) {
                //carnetCommande.envoyerCourrielConfirmation(commande, unClient);
                callback();
            });

        });
    },



    getCommandePrete : function(callback){
        dataBaseManager.getCommandePrete(callback)

    },
    getCommandePasPretePourRestaurateur : function(restaurateur, callback){
        restaurantManager.getListeRestaurantPourRestaurateur(restaurateur, function(liste){

            dataBaseManager.getCommandePasPretePourListeRestaurant(liste, callback)

        })
    },

    changerStatutCommande : function(ID, status, callback){
        dataBaseManager.getCommande(ID, function(maCommande){
            monClient = userManager.getUser(maCommande.client, function(monClient){
               // carnetCommande.envoyerMessage(monClient.telephone, status);
                dataBaseManager.changerStatusCommande(ID, status, callback);

            });

        });



    },
    getCommande : function(ID, callback){
        dataBaseManager.getCommande(ID,function(commande){
            callback(commande);
        });
    },

    getCommandePasPrete : function(callback){
        dataBaseManager.getCommandePasPrete(callback)

    },


    etampeCommande : function(ID, callback){
console.log('dans carnet');
	dataBaseManager.etampeCommande(ID, callback)
    },
    envoyerMessage: function(numero, status){
        //Code emprunté de https://www.twilio.com/docs/node/install
        //auteur inconnu
        var accountSid = 'AC25857fa1af8e6aa565b60bf43df28188';
        var authToken = "e7557f05f53edf5b4bc48e85785218fd";
        var client = require('twilio')(accountSid, authToken);
        client.messages.create({
            body: "*** Votre commande est passé au status: "+status+" ***",
            //to: "+1"+numero,
            to: "+14388629352",
            from: "14502310809"
        }, function(err, message) {
            process.stdout.write(message.sid);
            done();

        });
        //fin du code emprunté
	},
	
    envoyerCourrielConfirmation: function(commande, client){


        var texte = "BONJOUR "+client.nom+"\nVOTRE COMMANDE A ÉTÉ ENREGISTRÉE.\n+ADRESSE: "+commande.adresseLivraison+"\nTELEPHONE: "+client.telephone+"\nDATE DE LIVRAISON: "+commande.dateLivraison+"\nPLATS:\n "; // plaintext body

        var nodemailer = require('nodemailer');
        var mg = require('nodemailer-mailgun-transport');


        plats = commande.plats;

        plats.forEach(function(entry){
            texte = texte+entry.plat.nom+" quantité: "+entry.quantite+" prix: "+entry.getTotal()+" \n"
        });

        texte = texte+"total: "+commande.getTotal();

        // This is your API key that you retrieve from www.mailgun.com/cp (free up to 10K monthly emails)
            var auth = {
                auth: {
                    api_key: "key-7be7164ec84b683e17468dec6f249a7b",
                    domain: "sandbox1cc2ca3e2f6e420880a56d0965a877b1.mailgun.org"
                }
            }

        var nodemailerMailgun = nodemailer.createTransport(mg(auth));

        nodemailerMailgun.sendMail({
            from: "postmaster@sandbox1cc2ca3e2f6e420880a56d0965a877b1.mailgun.org",
            to: 'francois.laplante.1@etsmtl.net', // An array if you have multiple recipients.
            subject: 'VOTRE COMMANDE A ÉTÉ ENREGISTRÉE',
            text: texte
        }, function (err, info) {
            if (err) {
                console.log('Error: ' + err);
            }
            else {
                console.log('Response: ' + info);
            }
        });




    }

}

exports.carnetCommande = carnetCommande;
