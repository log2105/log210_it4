/**
 *
 */
var dataBaseManager = require("./DatabaseManager");

var menuManager = {
		
		ajouterMenu : function(menu,  callback){

			if(this.valider(menu)){

				dataBaseManager.ajouterMenu(menu, function(){
					
					callback("Menu ajouté");
				});
				
			}
			else{
				callback("Erreur: menu invalide.");
			};
			
		},
		
		modifierMenu : function(menu, callback){
			if(this.valider(menu)){
				
				dataBaseManager.modifierMenu(menu, function(){
                    callback("Menu modifié");
                })

        }
        else{
                callback("Erreur: menu invalide.");
            }

			
		},
		supprimerMenu : function(restaurant, nom, callback){

			dataBaseManager.supprimerMenu(restaurant, nom, function(resultat){
					callback(resultat)
				})
			
		},
		getMenu : function(restaurant, nom, callback){

			dataBaseManager.getMenu(restaurant, nom, function(menuInfo){
                		var unMenu = new menu(menuInfo.nom);

                		menuInfo.plat.forEach(function(entry) {
                    	var unPlat = new plat(entry.nom, entry.description, entry.prix);
                    unMenu.ajouterPlat(unPlat);
                });
				callback(unMenu)
			})
		},
		
		getNomMenu : function(restaurant, callback){
			
			dataBaseManager.getNomMenu(restaurant, function(noms){
				callback(noms);
			});
		},
		
		valider: function(menu){
			return true;
		}
		
		
}


function menu(nom, restaurant){
	this.nom = nom;
    this.restaurant = restaurant;
	this.plat = [];
	this.ajouterPlat = function(unPlat){
		this.plat.push(unPlat)
	}
	this.enleverPlat = function(position){
		if(i >= 0) {
			array.splice(i, 1);
		}
	}
};

function plat(nom, description, prix){
	this.nom = nom;
	this.description = description;
	this.prix = prix
}

exports.menuManager = menuManager;
