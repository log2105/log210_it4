var express = require('express');
var router = express.Router();

var bodyParser = require('body-parser');
var session = require('express-session');
var request = require('request');
var path = require('path');
var cookieParser = require('cookie-parser');

var databaseManager = require('../DatabaseManager');
var userManager = require('../UserManager').userManager;
var menuManager = require('../MenuManager').menuManager;
var restaurantManager = require('../RestaurantManager').restaurantManager;
var userManager = require('../UserManager').userManager;

var user

router.get('/passerCommande', function(req, res) {
	user = req.session.user;	
console.log(user);
	res.render("passerCommande",{nom: user.nom, date: user.dateNaissance, adresse: user.adresse, telephone: user.telephone, motDePasse: user.motDePasse, courriel: user.courriel});
	
});

//retourne une liste de menus
router.post('/getNomMenu', function(req, res) {
    menuManager.getNomMenu(req.body.restaurant, function(noms){
        res.send(noms);
    });

});

//retourne une liste de menus
router.post('/inscrireCommande', function(req, res) {
    databaseManager.ajouterCommande(req.body.commande, function(){
        res.send();
    });

});

//retourne une liste de plats selon le menu et restaurant
router.post('/getMenu', function(req, res) {
console.log(req.body);
    menuManager.getMenu(req.body.restaurant, req.body.nom, function(plats){
        res.send(plats);
    });

});

//retourne un client
router.post('/getClient', function(req, res) {
    userManager.getInfo(user.courriel, function(client){
        res.send(client);
    });

});

//retourne un carnet d'adresse
router.post('/getCarnetAdresse', function(req, res) {
    userManager.getInfo(req.body.ID, function(carnet){
        res.send(carnet);
    });

});

module.exports = router;
