var express = require('express');
var router = express.Router();

var bodyParser = require('body-parser');
var session = require('express-session');
var request = require('request');
var path = require('path');

var databaseManager = require('../DatabaseManager');
var userManager = require('../UserManager').userManager;
var menuManager = require('../MenuManager').menuManager;
var restaurantManager = require('../RestaurantManager').restaurantManager;


/* GET home page. */
/**
 * Route utilisée pour envoyé la page de gestion de compte
 */
router.get('/', function(req, res) {
	if(req.session.user){
		var user = req.session.user;
		 res.render("gestionCompte",{nom: user.nom, date: user.dateNaissance, adresse: user.adresse, telephone: user.telephone, motDePasse: user.motDePasse, courriel: user.courriel});
	}
	else {
		var filepath = path.resolve("./public/html/index.html")
		res.sendFile(filepath);
			} 
	});

/**
 * Route utilisée pour recevoir les modification à faire à un compte d'utilisateur
 */

router.post('/', function(req, res) {
	
	userManager.modifierUtilisateur(req.session.user.courriel, req.body.adresse, req.body.telephone, req.body.motDePasse, function(user){
		if(!user){
			res.render("message",{message: "une erreur est survenue dans la modification de compte"});
		}
		else{
			  req.session.user = user;
			  res.render("userInfo",{message: "Votre compte a été modifié!", nom: user.nom, adresse: user.adresse, telephone: user.telephone, date: user.dateNaissance, courriel: user.courriel});
		}	
		
	});
	});

module.exports = router;
