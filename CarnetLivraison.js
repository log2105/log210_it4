/**
 * Created by Frank on 2015-03-21.
 */

var dataBaseManager = require("./DataBaseManager");
var fabriqueLivraison = require("./public/javascript/FabriqueLivraison").FabriqueLivraison;

var carnetLivraison = {

    inscrireLivraison : function(uneCommande, livreur, callback ){
        var uneLivraison = fabriqueLivraison.creerLivraison(uneCommande, livreur);

        dataBaseManager.inscrireLivraison(uneLivraison, callback);

    },

    getLivraison : function(ID, callback) {

        dataBaseManager.trouverLivraison(ID, function (uneLivraison) {
            callback(uneLivraison);
        })
    },

    getLivraisonPourLivreur : function(livreur, callback){
        dataBaseManager.trouverLivraisonPourLivreur(livreur, function (tableauLivraison) {
            callback(tableauLivraison);
        })
    }


}

exports.carnetLivraison = carnetLivraison;