/**
 * 
 */
var dataBaseManager = require("./DatabaseManager");

var restaurantManager = {
		
		ajouterRestaurant : function(restaurant, callback){
			if(this.valider(restaurant)){
				
				dataBaseManager.ajouterRestaurant(restaurant, function(){
					callback("restaurant ajouter");
				});
				
			}
			else{
				callback("Erreur: restaurant invalide.");
			}
		},
		
		modifierRestaurant : function(restaurant, callback){
			if(this.valider(restaurant)){
				
				dataBaseManager.modifierRestaurant(restaurant, callback);

			}
			else{callback("Erreur: restaurant invalide.")}	;

		},
		supprimerRestaurant : function(ID, callback){
			
			dataBaseManager.supprimerRestaurant(ID, function(resultat){
					callback(resultat)
				})
			
		},
		getRestaurant : function(ID, callback){
			dataBaseManager.getRestaurant(ID, function(info){
				var monRestaurant = new restaurant(info.nom, info.adresse, info.telephone, info.restaurateur, info.ID);
				callback(monRestaurant)
			});
		},

    getListeRestaurantPourRestaurateur : function(restaurateur, callback){

			dataBaseManager.getRestaurantID(restaurateur, function(ID){
				callback(ID)
			});
			
		},
		
		getListeRestaurant : function(callback){
            var listeRestaurant = [];
			dataBaseManager.getListeRestaurant(function(liste){
                liste.forEach(function(entry){

                    var resto = new restaurant(entry.nom, entry.adresse, entry.telephone, entry.restaurateur, entry.ID);
                    listeRestaurant.push(resto);
                })

				callback(listeRestaurant)
			});
		},
        changerRestaurateur : function(ID, restaurateur, callback){

            this.getRestaurant(ID, function(unRestaurant){
                unRestaurant.setRestaurateur(restaurateur)
                restaurantManager.modifierRestaurant(unRestaurant, callback)
            });

        },
		
		valider: function(restaurant){
			return true;
		}
		
		
}

function restaurant(nom, adresse, telephone, restaurateur,ID){
    this.nom = nom;
    this.adresse = adresse;
    this.telephone = telephone;
    this.ID = ID;
    this.restaurateur = restaurateur;
    this.setRestaurateur = function(unRestaurateur){
        this.restaurateur = unRestaurateur;
    };


};

exports.restaurantManager = restaurantManager;
