/**
 * Created by Frank on 2015-03-03.
 */
var assert = require('assert');
var restaurantManager = require("../RestaurantManager.js").restaurantManager;
var dataBaseManager = require("../DataBaseManager");
var RestaurantManager = require("../RestaurantManager").RestaurantManager;
var fabriqueRestaurant = require("../public/javascript/FabriqueRestaurant").FabriqueRestaurant;

describe('FabriqueRestaurant', function() {

    describe('creerRestaurant()', function () {
        it('fabrique à Restaurant', function (done) {
            var monRestaurant = fabriqueRestaurant.creerRestaurantComplet("Le chateua", "321 vrai rue", "123-132-1233", "admin@admin",12);

            assert.equal("Le chateua", monRestaurant.nom);
            assert.equal("321 vrai rue", monRestaurant.adresse);
            assert.equal("123-132-1233", monRestaurant.telephone);
            assert.equal("admin@admin", monRestaurant.restaurateur);
            assert.equal(12, monRestaurant.ID);
            monRestaurant.setRestaurateur("Bob@bob");
            assert.equal("Bob@bob", monRestaurant.restaurateur);
            done();
        });

    });


});