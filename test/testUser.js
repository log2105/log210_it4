/**
 * Created by Frank on 2015-02-28.
 */
var assert = require('assert');
var restaurantManager = require("../RestaurantManager.js").restaurantManager;
var dataBaseManager = require("../DataBaseManager");
var menuManager = require("../MenuManager").menuManager;
var userManager = require('../UserManager').userManager;

describe('restaurateurs', function() {

    describe('getListeRestaurateurs()', function () {
        it('get a liste of restaurateurs', function (done) {
            userManager.getListeRestaurateur(function(liste){
                assert.equal("resto@resto", liste[0].courriel);
                assert.equal("Jean Bob", liste[0].nom);
                assert.equal("Test012", liste[1].courriel);
                assert.equal("Test012 Test012", liste[1].nom);
                done();
            })

        });

    });


});