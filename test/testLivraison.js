/**
 * Created by Frank on 2015-03-21.
 */

var assert = require('assert');
var fabriqueLivraison = require("../public/javascript/FabriqueLivraison").FabriqueLivraison;
var fabriqueCommande = require("../public/javascript/FabriqueCommande").FabriqueCommande;

var carnetLivraison = require("../CarnetLivraison").carnetLivraison;
var carnetCommande = require("../CarnetCommande").carnetCommande;

describe('livraison', function() {

    describe('inscrireLivraison)', function () {
        it('inscrit une livraison', function (done) {

            var mesPlats = [{plat: {nom: "truc", description: "dutruc", prix: 3}, quantite: 2},{plat: {nom: "chose", description: "duchose", prix: 4}, quantite: 3}];
            var maCommande = {  restaurant: 2,
                ID: 3,
                client: "test@test",
                adresseLivraison: "123123 rue du paspire",
                menu: "matin",
                plats: mesPlats,
                status: "Accepté"
            };


            carnetLivraison.inscrireLivraison(maCommande, "unLivreur", function(resultat){
                carnetLivraison.getLivraison(1, function(uneLivraison){

                    assert.equal(1, uneLivraison.ID);
                    assert.equal(3, uneLivraison.commande);
                    assert.equal("unLivreur", uneLivraison.livreur);
                    done();
                })

            });


        });
        it('recherche une liste de livraison', function (done) {


            carnetLivraison.getLivraisonPourLivreur("unLivreur", function(resultat){

                assert.equal(1, resultat[0].ID);
                assert.equal(3, resultat[0].commande);
                assert.equal("unLivreur", resultat[0].livreur);

                assert.equal(2, resultat[1].ID);
                assert.equal(4, resultat[1].commande);
                assert.equal("unLivreur", resultat[1].livreur);
                done();
            });


        });
    });


});