var assert = require('assert');
var restaurantManager = require("../RestaurantManager.js").restaurantManager;





describe('restaurantManager', function(){

    describe('ajouterRestaurant()', function(){
        it('ajoute un restaurant', function(done){
            var resto = new restaurant("Le gascon qui sourit", "2321, rue des cdmanger", "426-932-3233", "resto@resto");
            restaurantManager.ajouterRestaurant(resto, function(){
                done();
            });
        });
    });

    describe('getRestaurant()', function(){
        it('obtient un restaurant', function(done){
            restaurantManager.getRestaurant(1, function(resto){

                assert.equal(1, resto.ID);
                assert.equal("LeBon", resto.nom);
                done();
            });
        });
    });
    describe('modifierRestaurant()', function(){
        it('modifie un restaurant', function(done){
            var resto = new restaurant("Le chateau port 234", "340, rue des oper", "123-965-4543", "resto@resto", 3);
            restaurantManager.modifierRestaurant(resto, function(){
                restaurantManager.getRestaurant(3, function(resto){

                    assert.equal(3, resto.ID);
                    assert.equal("Le chateau port 234", resto.nom);
                    done();
                });
            });
        });
    });
    describe('getRestaurant()', function(){
        it('obtient un restaurant', function(done){
            restaurantManager.getRestaurant(2, function(resto){
                assert.equal(2, resto.ID);
                assert.equal("La maison violette", resto.nom);
                done();
            });
        });
    });
    describe('getListeRestaurant()', function(){
        it('obtient une liste de restaurants', function(done){
            restaurantManager.getListeRestaurant(function(liste){
                assert.equal(2, liste[1].ID);
                assert.equal("La maison violette", liste[1].nom);
                assert.equal(7, liste[6].ID);
                assert.equal("La chève robuste", liste[6].nom);
                done();
            });
        });
    });
    describe('supprimerRestaurant()', function(){
        it('supprime un restaurant', function(done){
            restaurantManager.supprimerRestaurant(10, function(liste){
                done();
            });
        });
    });
    describe('getRestoID()', function(){
        it('obtient une liste de restaurants pour un restaurateur', function(done){
            restaurantManager.getListeRestaurantPourRestaurateur("Test012",function(liste){
                assert.equal(1, liste[0].ID);
                assert.equal("LeBon", liste[0].nom);


                done();
            });
        });
        it('obtient une liste de restaurants pour un restaurateur', function(done){
            restaurantManager.getListeRestaurantPourRestaurateur("resto@resto",function(liste){
                assert.equal(2, liste[0].ID);
                assert.equal("La maison violette", liste[0].nom);
                assert.equal(7, liste[5].ID);
                assert.equal("La chève robuste", liste[5].nom);


                done();
            });
        });
        it('blah blah modifier le restaurateur', function(done){
            restaurantManager.changerRestaurateur(8,"Test012", function(message){
                restaurantManager.getRestaurant(8, function(unRestaurant){
                    assert.equal(8, unRestaurant.ID);
                    assert.equal("Test012", unRestaurant.restaurateur);
                    done()

                })

            });
        });
        it('blah blah modifier le restaurateur', function(done){
            restaurantManager.changerRestaurateur(8,"resto@resto", function(message){
                restaurantManager.getRestaurant(8, function(unRestaurant){
                    assert.equal(8, unRestaurant.ID);
                    assert.equal("resto@resto", unRestaurant.restaurateur);
                    done()

                })

            });
        });
    });
});

function restaurant(nom, adresse, telephone, restaurateur,ID){
    this.nom = nom;
    this.adresse = adresse;
    this.telephone = telephone;
    this.ID = ID;
    this.restaurateur = restaurateur;
    this.setRestaurateur = function(unRestaurateur){
        this.restaurateur = restaurateur;
    };


};