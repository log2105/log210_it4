/**
 * Created by Frank on 2015-02-27.
 */
/**
 * Created by Frank on 2015-02-26.
 */
var assert = require('assert');
var restaurantManager = require("../RestaurantManager.js").restaurantManager;
var dataBaseManager = require("../DataBaseManager");
var menuManager = require("../MenuManager").menuManager;

describe('menuManager', function(){

    describe('ajouterMenu()', function(){
        it('ajoute un menu', function(done){
            var unMenu = new menu("souper26",7);
            var unPlat1 = new plat("poulet","Du bon poulet", 30);
            var unPlat2 = new plat("eggroll","Du bon eggroll", 10);
            var unPlat3 = new plat("jambon","Du bon jambon", 20);
            unMenu.ajouterPlat(unPlat1);
            unMenu.ajouterPlat(unPlat2);
            unMenu.ajouterPlat(unPlat3);
            
            menuManager.ajouterMenu(unMenu, function(){
                
                menuManager.getMenu(7, "souper26", function(nouveauMenu){

                    assert.equal("souper26", nouveauMenu.nom);
                    assert.equal("poulet", nouveauMenu.plat[2].nom);
                    menuManager.supprimerMenu(7, "souper26", function(){
                        done();
                    })
                })

            })
        });
        describe('modifierMenu()', function(){
            it('modifier un menu', function(done){
                var unMenu = new menu("testModif", 7);
                var unPlat1 = new plat("soupe","Du bon soupe", 30);
                var unPlat2 = new plat("salade","Du bon salade", 10);
                var unPlat3 = new plat("omelette","Du bon omelette du fromage", 20);
                unMenu.ajouterPlat(unPlat1);
                unMenu.ajouterPlat(unPlat2);
                unMenu.ajouterPlat(unPlat3);

                menuManager.ajouterMenu(unMenu, function(){

                    var unMenu2 = new menu("testModif",7);
                    var unPlat21 = new plat("soupe ++","Du bon soupe vraiment bon", 30);
                    var unPlat22 = new plat("salade ++","Du bon salade vraiment bon", 10);
                    var unPlat23 = new plat("omelette ++","Du bon omelette du fromage", 20);
                    unMenu2.ajouterPlat(unPlat21);
                    unMenu2.ajouterPlat(unPlat22);
                    unMenu2.ajouterPlat(unPlat23);

                    menuManager.modifierMenu(unMenu2,function(){
                        menuManager.getMenu(7, "testModif", function(modifMenu){

                            assert.equal("testModif", modifMenu.nom);
                            assert.equal("soupe ++", modifMenu.plat[2].nom);

                            menuManager.supprimerMenu(7, "testModif", function(){
                                done();
                            });

                        });

                    });
                });
                });
            });
    });




});

function menu(nom, restaurant){
    this.nom = nom;
    this.restaurant = restaurant;
    this.plat = [];
    this.ajouterPlat = function(unPlat){
        this.plat.push(unPlat)
    }
    this.enleverPlat = function(position){
        if(i >= 0) {
            array.splice(i, 1);
        }
    }
};

function plat(nom, description, prix){
    this.nom = nom;
    this.description = description;
    this.prix = prix
}

exports.menuManager = menuManager;