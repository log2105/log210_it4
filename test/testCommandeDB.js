/**
 * Created by Frank on 2015-03-03.
 */

var dataBaseManager = require("../DataBaseManager");
var assert = require('assert');
var fabriqueCommande = require("../public/javascript/FabriqueCommande").FabriqueCommande;
var carnetCommande = require("../CarnetCommande").carnetCommande;

describe('commande', function() {

    describe('commande()', function () {
        it('commande', function (done) {
            var mesPlats = [{plat: {nom: "truc", description: "dutruc", prix: 3}, quantite: 2},{plat: {nom: "chose", description: "duchose", prix: 4}, quantite: 3}];
            var maCommande = {  restaurant: 2,
                                client: "test@test",
                                adresseLivraison: "123123 rue du paspire",
                                menu: "matin",
                                plats: mesPlats,
                                status: "Accepté"

                                }

            dataBaseManager.ajouterCommande(maCommande, function(){

                dataBaseManager.getCommande(2, function(uneCommande){

                    assert.equal(2,uneCommande.restaurant );
                    assert.equal("test@test",uneCommande.client );
                    assert.equal("123123 rue du paspire",uneCommande.adresseLivraison );
                    assert.deepEqual(mesPlats, uneCommande.plats);
                    assert.equal("matin", uneCommande.menu );
                    assert.equal("En préparation", uneCommande.status );
                    done();
                });
            });

        });

    });
    describe('commande()', function () {
        it('commande2', function (done) {
           var mesPlats = [{plat: {nom: "truc", description: "dutruc", prix: 3}, quantite: 2},{plat: {nom: "chose", description: "duchose", prix: 4}, quantite: 3}];
           // var maCommande = fabriqueCommande.creerCommande(2, "test@test", "3791 rue du lion", "matin", mesPlats);

            //dataBaseManager.ajouterCommande(maCommande, function(){

                dataBaseManager.getCommandePasPrete(function(liste){
                    assert.equal(2,liste[1].restaurant );
                    assert.equal("test@test",liste[1].client );
                    assert.equal("123123 rue du paspire",liste[1].adresseLivraison );
                    assert.deepEqual(mesPlats, liste[1].plats);
                    assert.equal("matin", liste[1].menu );
                    assert.equal("En préparation", liste[1].status );
                    done();

                });
            //});

        });

    });
    describe('commande()', function () {
        it('commande3', function (done) {

           carnetCommande.getCommandePasPretePourRestaurateur("resto@resto", function(resultats){

               assert.equal(2,resultats[1].restaurant );
               assert.equal("test@test",resultats[1].client );
               assert.equal("123123 rue du paspire",resultats[1].adresseLivraison );
               assert.equal("29 rue du paspire",resultats[5].adresseLivraison );

               done();
           })

        });

    });
    describe('commande()', function () {
        it('commande4', function (done) {

            carnetCommande.getCommandePrete(function(resultats){

                assert.equal(13,resultats[0].ID );
                assert.equal("test@test",resultats[0].client );
                assert.equal(15,resultats[1].ID );
                assert.equal("test@test001",resultats[1].client );
                done();
            })

        });

    });
    describe('testChangementStatusCommande', function () {
        it('changerStatusCommande', function (done) {

            carnetCommande.getCommandePasPrete(function(mesCommandes){
                var ID = mesCommandes[1].ID;
                carnetCommande.changerStatutCommande(ID, "En préparation",function(){

                    carnetCommande.getCommande(ID, function(maCommande){

                        assert.equal("En préparation",maCommande.status);
                        setTimeout(done(),1000);
                    });
                });

            });


        });

    });

});