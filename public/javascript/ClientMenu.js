/**
 * ClientMenu
 */

function ClientMenu(){
	
	this.menuEnCreation = null;
	
	this.creerMenu = function(nom){
		
		this.menuEnCreation = new menu(nom);
		alert('Choisissez maintenant un restaurant a associer au menu '+nom+'.');
		
	};
	
	this.ajouterPlat = function(nom, description, prix){
		
		unPlat = new plat(nom, description, prix);
		this.menuEnCreation.ajouterPlat(unPlat);
		
	};
	
	this.confirmerMenu = function(callback){
		var menu = this.menuEnCreation;
		$.ajax({
			type: 'POST',
			url: '/restaurateur/ajouterMenu',
			data: JSON.stringify(menu),
			contentType: 'application/json',
			dataType: "json",
			success: function(data) {
				console.log(JSON.stringify(data));
				callback(JSON.stringify(data));
                                this.menuEnCreation = null;
                                location.reload();
			}
		});
	}

	this.getMenu = function(restaurant, nom, callback){
		$.ajax({
		    type: 'POST',
		    data: {'restaurant':restaurant, 'nom':nom},
		    url: '/client/getMenu/'
			}).done(function(data) {
				callback(data);
		});
	}

        this.ajouterRestaurant = function(idResto){
            if(!this.menuEnCreation){
                alert('Nommer le menu en premier.');
            }else{
                this.menuEnCreation.ajouterRestaurant(idResto);
                alert('Vous pouvez maintenant ajouter des plats au menu.');
            }
        }

	this.getNomMenu = function(restaurant, callback){
	    	$.ajax({
		    type: 'POST',
		    data: {'restaurant':restaurant},
		    url: '/client/getNomMenu/'
			}).done(function(data) {
				callback(data);
		});
	};
	
};

function menu(nom){
	this.nom = nom;
	this.plat = [];
        this.restaurant = '';
        this.ajouterRestaurant = function(idResto){
                this.restaurant = idResto;
        }
	this.ajouterPlat = function(unPlat){
		this.plat.push(unPlat);
	}
	this.enleverPlat = function(position){
		if(i >= 0) {
			array.splice(i, 1);
		}
	}
};

function plat(nom, description, prix){
	this.nom = nom;
	this.description = description;
	this.prix = prix
};
