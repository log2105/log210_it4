/**
 * Created by Frank on 2015-03-07.
 */

(function() {
	var FabriqueCommande = {
	    
	    creerCommandeVide: function(restaurant, client, menu){

		var commande = {
		    ID: "",
		    restaurant: restaurant,
		    client: client,
		    adresseLivraison: "",
		    dateLivraison: "",
		    menu: menu,
		    plats: [],
		    statut: "En creation",
		    ajouterPlat: function(plat , quantite){
		        lignePlat = FabriqueLigneCommande.creerLigneCommande(plat , quantite)
		        this.plats.push(lignePlat)
		    },
		    getTotal: function(){
		        var total = 0;
		        this.plats.forEach(function(entry){
		            total = total + entry.getTotal()
		        });
		        return total;
		    },
		    setDate: function(date){
		        this.dateLivraison = date;
		    },
		    setAdresse: function(adresse){
		        this.adresseLivraison = adresse;
		    }
		}



		return commande
	    },

	
		creerCommandeComplete: function(ID,restaurant, client, adresseLivraison, menu, plats, status, date){
		var commande = {
		    ID: ID,
		    restaurant: restaurant,
		    client: client,
		    adresseLivraison: adresseLivraison,
		    dateLivraison: date,
		    menu: menu,
		    plats: [],
		    status: status,
		    ajouterPlat: function(plat , quantite){
		        lignePlat = FabriqueLigneCommande.creerLigneCommande(plat , quantite)
		        this.plats.push(lignePlat)
		    },
		    getTotal: function(){
		        var total = 0;
		        this.plats.forEach(function(entry){
		            total = total + entry.getTotal()
		        });
		        return total;
		    },
		    setDate: function(date){
		        this.dateLivraison = date;
		    },
		    setAdresse: function(adresse){
		        this.adresseLivraison = adresse;
		    }

		    }
		plats.forEach(function(entry){
		    commande.ajouterPlat(entry.plat, entry.quantite)
		});

		return commande
	    },



	    creerCommande : function(restaurant, client, adresseLivraison, menu, plats, date) {

		var commande = {
		        ID: "",
		        restaurant: restaurant,
		        client: client,
		        adresseLivraison: adresseLivraison,
			dateLivraison: date,
		        menu: menu,
		        plats: [],
		        statut: "",
		                        ajouterPlat: function(plat , quantite){
		            lignePlat = FabriqueLigneCommande.creerLigneCommande(plat , quantite)
		            this.plats.push(lignePlat)
		        },
		        getTotal: function(){
		            var total = 0;
		            this.plats.forEach(function(entry){
		                total = total + entry.getTotal()
		            });
		            return total;
		        },
		        setDate: function(date){
		            this.dateLivraison = date;
		        },
		        setAdresse: function(adresse){
		            this.adresseLivraison = adresse;
		        }
		    }

		plats.forEach(function(entry){
		    commande.ajouterPlat(entry.plat, entry.quantite)
		});

		return commande
	    },


	    creerCommandeComplete : function(ID,restaurant, client, adresseLivraison, menu, plats, status){

		var commande = {
		    ID: ID,
		    restaurant: restaurant,
		    client: client,
		    adresseLivraison: adresseLivraison,
		    menu: menu,
		    plats: plats,
		    status: status,
		    ajouterPlat: function(plat){
		        this.plats.push(plat)
		    }

		}

		return commande
	    }
	}
	
	var FabriqueLigneCommande = {

		creerLigneCommande: function(plat , quantite){		
		ligneCommande = {
			plat: plat,
			quantite: quantite,
			getTotal: function(){
				return (plat.prix*quantite)
			}
		}
		return ligneCommande;
		}
	}

	if(typeof module !== 'undefined' && typeof module.exports !== 'undefined'){
		module.exports = FabriqueCommande;
	}else{
		window.FabriqueCommande = FabriqueCommande;
	}
})();

