var clientRestaurant = new ClientRestaurant();
var listeRestaurateurs;
var listeRestaurants;

$(document).ready(function() {
    $('#btnAjouterInfoResto').on('click', ajoutResto1);
    $('#btnConfirmerAjout').on('click', ajoutResto2);
});


function ajoutResto1(event){

	var nom = $('#ajouterResto fieldset input#nomInput').val();
	var adresse = $('#ajouterResto fieldset input#adresseInput').val();
	var numeroTelephone = $('#ajouterResto fieldset input#numeroTelephoneInput').val();

	if(nom != '' && adresse != '' && numeroTelephone != ''){
			clientRestaurant.creerRestaurant(nom, adresse, numeroTelephone, function(data){
				clientRestaurant.getListeRestaurateurs( function(data) {
					listeRestaurateurs = data;		
					afficherRestaurateurs();
				});
			});
	} else{
		alert('Un ou plusieurs champs ne sont pas remplis');
	}

};

function ajoutResto2(event){
	clientRestaurant.ajouterRestaurateur($('#ajouterRestaurateur fieldset input#restaurateurInput').val(), function(data){
	alert('Le restaurant a été enregistré.');
	});
};

function afficherRestaurateurs() {
	var tableContent = '';

        $.each(listeRestaurateurs, function(){
            tableContent += '<tr>';
            tableContent += '<td>' + this.nom + '</td>';
            tableContent += '<td>' + this.courriel + '</td>';
            tableContent += '</tr>';
        });

        $('#restaurateurList table tbody').html(tableContent);

};
