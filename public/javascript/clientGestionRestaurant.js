var clientRestaurant = new ClientRestaurant();
var listeRestaurateurs;
var listeRestaurants;
var IDAModifier = '';

$(document).ready(function() {
    $('#restaurantsList table tbody').on('click', 'td a.linkdeleteresto', deleteResto);
    $('#restaurantsList table tbody').on('click', 'td a.linkmodifierresto', commencerModResto);
    $('#btnModInfoResto').on('click', modifierResto);
    afficherRestaurants();
});



function afficherRestaurants() {
    var tableContent = '';    

    clientRestaurant.getListeRestaurant(function(data){
	listeRestaurants = data;
    
        $.each(listeRestaurants, function(){
            tableContent += '<tr>';
            tableContent += '<td>'+this.nom + '</td>';
            tableContent += '<td>' + this.adresse + '</td>';
            tableContent += '<td>' + this.telephone + '</td>';
            tableContent += '<td>' + this.restaurateur + '</td>';
	    tableContent += '<td><a href="#" class="linkmodifierresto" rel="' + this.ID + '">Modifier</a></td>';
            tableContent += '<td><a href="#" class="linkdeleteresto" rel="' + this.ID + '">Supprimer</a></td>';
            tableContent += '</tr>';
        });
        $('#restaurantsList table tbody').html(tableContent);
    });
};

function commencerModResto(event) {
    IDAModifier = $(this).attr('rel');
    alert('Restaurant '+$(this).attr('rel')+' prêt à être modifié.');
    clientRestaurant.getRestaurant(IDAModifier, function(result){
        $('#nomInput').val(result.nom);
        $('#adresseInput').val(result.adresse);
        $('#numeroTelephoneInput').val(result.telephone);
        $('#restaurateurInput').val(result.restaurateur);
    });
}

function deleteResto(event) {

    event.preventDefault();

    // dialog de confirmation
    var confirmation = confirm('Voulez-vous supprimer ce restaurant?');

    // Si l'utilisateur a confirme
    if (confirmation === true) {

        $.ajax({
            type: 'POST',
	    data: {'ID':$(this).attr('rel')},
            url: '/admin/supprimerRestaurant/'
        }).done(function( response ) {

            alert('Restaurant supprimé.');

            // Update tableau
            afficherRestaurants();

        });

    }
    else {
        return false;
    }
};

function modifierResto(event) {

    event.preventDefault();

    var confirmation = confirm('Voulez-vous modifier le restaurant '+IDAModifier+'?');

    // Si l'utilisateur a confirme
    if (confirmation === true) {
		var unRestaurant = { 
			'nom': $('#restaurantModification fieldset input#nomInput').val(), 
			'adresse': $('#restaurantModification fieldset input#adresseInput').val(), 
			'telephone': $('#restaurantModification fieldset input#numeroTelephoneInput').val(), 'restaurateur':	$('#restaurantModification fieldset input#restaurateurInput').val(), 'ID' : IDAModifier	
		}

        $.ajax({
            		type: 'POST',
            		data: unRestaurant,
            		url: '/admin/modifierRestaurant',
            		dataType: 'JSON'
        }).done(function( response ) {

            alert('Restaurant modifié.');

            // Update tableau
            afficherRestaurants();

        });

    }
    else {
        return false;
    }
};
