function ClientCommande(){
	this.setStatut = function(ID, statut, callback){
		        $.ajax({
			    type: 'POST',
			    data: {'ID':ID,'statut':statut},
			    url: '/restaurateur/changerStatutCommande/'
			}).done(function( response ) {
				callback();
			});
	}

	this.getListeCommandePasPretePourRestaurateur = function(callback){
			$.get('/restaurateur/getCommandePasPretePourRestaurateur', function(data) {
				result = data;
				callback(result);             
			});
	}

	this.getListeCommandePasPrete = function(callback){
			$.get('/restaurateur/getCommandePasPrete', function(data) {
				result = data;
				callback(result);             
			});
	}

	this.getListeCommandePrete = function(callback){
			$.get('/restaurateur/getCommandePrete', function(data) {
				result = data;
				callback(result);             
			});
	}

	this.getCommande = function(ID, callback){
		$.ajax({
		    type: 'POST',
		    data: {'ID':ID},
		    url: '/restaurateur/getCommande/'
			}).done(function(data) {
				callback(data);
		});
	}

	this.etampeCommande = function(ID, callback){
		        $.ajax({
			    type: 'POST',
			    data: {'ID':ID},
			    url: '/restaurateur/etampeCommande/'
			}).done(function( response ) {
				callback();
			});
	}
	
	this.getCarnetAdresse = function(ID, callback){
		        $.ajax({
			    type: 'POST',
			    data: {'ID':ID},
			    url: '/client/getCarnetAdresse/'
			}).done(function(carnet) {
				callback(carnet);
			});
	}

}
