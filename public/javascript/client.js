var clientRestaurant = new ClientRestaurant();
var listeRestaurateurs;
var listeRestaurants;
var besoinRestaurateurs='false';
var besoinRestaurants='false';

$(document).ready(function() {
    $('#btnAjouterInfoResto').on('click', ajoutResto1);
    $('#btnConfirmerAjout').on('click', ajoutResto2);
    $('#restaurants table tbody').on('click', 'td a.linkdeleterestaurant', deleteResto);
    $('#btnModInfoResto').on('click', modifierResto);
	//facon de checker que l'on est dans la page avec les restaurateurs	
	if(besoinRestaurateurs=='true'){
		afficherRestaurateurs();
	};
	if(besoinRestaurants=='true'){
		afficherRestaurants();
	};
});


function ajoutResto1(event){

var nom = $('#ajouterResto fieldset input#nomInput').val();
var adresse = $('#ajouterResto fieldset input#adresseInput').val();
var numeroTelephone = $('#ajouterResto fieldset input#numeroTelephoneInput').val();

if(nom != '' && adresse != '' && numeroTelephone != ''){
		clientRestaurant.creerRestaurant(nom, adresse, numeroTelephone, function(data){
			clientRestaurant.getListeRestaurateurs( function(data) {
				listeRestaurateurs = data;		
				afficherRestaurateurs();
			});
		});
} else{
	alert('Un ou plusieurs champs ne sont pas remplis');
}

};

function ajoutResto2(event){
	clientRestaurant.ajouterRestaurateur($('#ajouterRestaurateur fieldset input#restaurateurInput').val(), function(data){
	alert('Le restaurant a été enregistré.');
	});
};

function afficherRestaurateurs() {
	var tableContent = '';
	//la fonction a ete appeler alors on a plus besoin des restaurateurs
	besoinRestaurateurs = 'false';
        $.each(listeRestaurateurs, function(){
            tableContent += '<tr>';
            tableContent += '<td>' + this.nom + '</td>';
            tableContent += '<td>' + this.courriel + '</td>';
            tableContent += '</tr>';
        });

        $('#restaurateurList table tbody').html(tableContent);

};

function afficherRestaurants() {

    clientRestaurant.getListeRestaurant(function(data){
		listeRestaurants = data;
	

        $.each(listeRestaurants, function(){
            tableContent += '<tr>';
            tableContent += '<td>'+this.nom + '</a></td>';
            tableContent += '<td>' + this.adresse + '</td>';
            tableContent += '<td>' + this.numeroTelephone + '</td>';
	    tableContent += '<td><a href="#" class="linkmodifierresto" rel="' + this.ID + '">Modifier</a></td>';
            tableContent += '<td><a href="#" class="linkdeleteresto" rel="' + this.ID + '">Supprimer</a></td>';
            tableContent += '</tr>';
        });

        // Inject the whole content string into our existing HTML table
        $('#restaurantsList table tbody').html(tableContent);
	});
};

function deleteResto(event) {

    event.preventDefault();

    // dialog de confirmation
    var confirmation = confirm('Voulez-vous supprimer ce restaurant?');

    // Si l'utilisateur a confirme
    if (confirmation === true) {

        $.ajax({
            type: 'DELETE',
            url: '/admin/supprimerRestaurant/' + $(this).attr('rel')
        }).done(function( response ) {

            // Check pour reponse null
            if (response.msg === '') {
            }
            else {
                alert('Error: ' + response.msg);
            }

            // Update tableau
            afficherRestaurants();

        });

    }
    else {
        return false;
    }
};

function modifierResto(event) {

    event.preventDefault();

    // dialog de confirmation
    var confirmation = confirm('Voulez-vous modifier ce restaurant?');

    // Si l'utilisateur a confirme
    if (confirmation === true) {
		var unRestaurant = { 
			'nom': $('#restauratantModification fieldset input#nomInput').val(), 
			'adresse': $('#restauratantModification fieldset input#adresseInput').val(), 
			'numeroTelephone': $('#restauratantModification fieldset input#numeroTelephoneInput').val()		
		}
        $.ajax({
            		type: 'POST',
            		data: unRestaurant,
            		url: '/admin/modifierRestaurant',
            		dataType: 'JSON'
        });

    }
    else {
        return false;
    }
};
