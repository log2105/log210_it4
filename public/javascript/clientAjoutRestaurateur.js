var clientRestaurateur = new ClientRestaurateur();
var listeRestaurants;
var restaurateur;

$(document).ready(function() {
    $('#btnAjouterInfoRestaurateur').on('click', enregistrer);
    $('#btnConfirmerAjout').on('click', ajoutRestaurateur);
    $('#restaurantsList table tbody').on('click', 'td a.linkregresto', regResto);
    afficherRestaurants();
});

function enregistrer() {
    var prenom = $('#ajouterResto fieldset input#prenomInput').val();
    var nom = $('#ajouterResto fieldset input#nomInput').val();
    var dateNaissance = $('#ajouterResto fieldset input#naissanceInput').val();
    var adresse = $('#ajouterResto fieldset input#adresseInput').val();
    var telephone = $('#ajouterResto fieldset input#numeroTelephoneInput').val();
    var courriel = $('#ajouterResto fieldset input#courrielInput').val();
    var motDePasse = $('#ajouterResto fieldset input#passwordInput').val();

    if(prenom != '' && nom != '' && dateNaissance != '' && adresse != '' && telephone != '' && courriel != '' && motDePasse != ''){

        clientRestaurateur.creerRestaurateur(prenom, nom, dateNaissance, adresse, telephone, courriel, motDePasse, function() {
           alert('Choisir des restaurants a assigner a ce restaurateur.');
        });
    }else{
	alert('Un ou plusieurs champs sont vide.');
    }
}

function regResto(event){
   ID = $(this).attr('rel');
   clientRestaurateur.setRestaurant(ID);
}

function ajoutRestaurateur(){
    clientRestaurateur.ajouterRestaurateur(function(){
        afficherRestaurants();
	alert('Restaurateur enregistre.');
    });
}

function afficherRestaurants() {
    var tableContent = '';    

    clientRestaurateur.getListeRestaurant(function(data){
	listeRestaurants = data;
    
        $.each(listeRestaurants, function(){
            tableContent += '<tr>';
            tableContent += '<td>'+this.nom + '</td>';
            tableContent += '<td>' + this.adresse + '</td>';
            tableContent += '<td>' + this.telephone + '</td>';
            tableContent += '<td>' + this.restaurateur + '</td>';
	    tableContent += '<td><a href="#" class="linkregresto" rel="' + this.ID + '">********</a></td>';
            tableContent += '</tr>';
        });
        $('#restaurantsList table tbody').html(tableContent);
    });
};

