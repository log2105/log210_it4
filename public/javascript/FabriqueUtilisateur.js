/**
 * Created by Frank on 2015-03-03.
 */
(function() {
	var FabriqueUtilisateur = {

		creerUtilisateur: function(nom, dateNaissance, adresse, telephone, courriel, motDePasse, type, carnetAdresse){


		var utilisateur = {
		        nom: nom,
		        dateNaissance: dateNaissance,
		        adresse: adresse,
		        telephone: telephone,
		        courriel: courriel,
		        motDePasse: motDePasse,
		        type: type,
		        carnetAdresse: carnetAdresse,

		        ajouterAdresse: function(uneAdresse){
		            if(this.carnetAdresse.indexOf(uneAdresse) == -1){
		                this.carnetAdresse.push(uneAdresse)
		            }
		        },

		        changerAdresseParDefaut:function(uneAdresse){
		            //si l'adresse est dans le carnet d'adresse, on l'enlève
		             if(this.carnetAdresse.indexOf(uneAdresse) != -1){
		                index = this.carnetAdresse.indexOf(uneAdresse)
		                 this.carnetAdresse.splice(index, 1);
		            }
		            if(carnetAdresse.indexOf(adresse) == -1){
		                this.carnetAdresse.push(adresse)
		                this.adresse = uneAdresse
		            }
		            else{
		                 this.adresse = uneAdresse;
		                }
		    }


		};
			if(this.carnetAdresse != null){
		    this.carnetAdresse.forEach(function(entry){
		        utilisateur.ajouterAdresse(entry);
		    });
		}
		if(type == "client"){utilisateur.homePage = "./public/html/NavigationHome.html"}
		else if(type == "restaurateur"){utilisateur.homePage = ""}
		else if(type == "livreur"){utilisateur.homePage = ""}
		else if(type == "admin"){utilisateur.homePage = ""}
		else{utilisateur.homePage = "./public/html/NavigationHome.html"}
			if(carnetAdresse != null){
		    carnetAdresse.forEach(function(entry) {
		        utilisateur.carnetAdresse.ajouterAdresse(entry)
		    });
		}
		else{
		    carnetAdresse = [];
		}

		
		return utilisateur;
	    }


	}

	if(typeof module !== 'undefined' && typeof module.exports !== 'undefined'){
		module.exports = FabriqueUtilisateur;
	}else{
		window.FabriqueUtilisateur = FabriqueUtilisateur;
	}
})();
